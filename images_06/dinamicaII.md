# Dinamica II

---

# Dinamica del moto circolare

* Abbiamo visto che in generale l'accelerazione, variazione della *velocità* rispetto al *tempo*, può essere scomposta in una componente **tangenziale** e una componente **normale** alla traiettoria. 
* Se un corpo cambia velocità nel tempo, ed è quindi soggetto ad una accelerazione, per la seconda legge di Newton ci deve essere una **forza** risultante $F$, tale che

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $\displaystyle  \sum \mathbf{F} = m \mathbf{a}$

---

# Dinamica del moto circolare uniforme

* Nel caso del moto circolare uniforme l'accelerazione punta verso il centro: **accelerazione centripeta**.

* Lo stesso deve fare la forza $F$, di conseguenza detta **forza centripeta**.

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $\displaystyle \vert\sum \mathbf{F}\vert = \frac{mv^2}{R}$


---

# Moto relativo

![center](motorelativo.png)

<sub>Quindi osservo la stessa accelerazione in entrambi i sistemi di riferimento.
  Ma se $V$ non fosse costante...</sub>

---

# Cenni sui sistemi non inerziali

$V$ non costante, senza rotazioni.

![center 150%](motorelativo2.png)


---

# Cenni sui sistemi non inerziali

Quindi, per un moto relativo di *trascinamento*, 

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $\mathbf{a^\prime} = \mathbf{a}-\mathbf{A}$, 

dove:
- $\mathbf{a^\prime}$ è l'accelerazione osservata nel sistema di riferimento **non inerziale** $x^\prime - y^\prime$, 
- $\mathbf{A}$ è l'accelerazione del sistema non inerziale rispetto al sistema inerziale, 
- $\mathbf{a}$ è l'accelerazione osservata nel sistema di riferimento inerziale.

---

# Cenni sui sistemi non inerziali

Per il secondo principio di Newton, poiché il sistema in cui si osserva $\mathbf{a}$ è inerziale, possiamo scrivere

$m\mathbf{a^\prime} = \sum \mathbf{F}-m\mathbf{A}$

Allora scopriamo che la massa del corpo moltiplicata per l'accelerazione osservata **non risulta uguale alla somma delle forze applicate!!**.

---

# Cenni sui sistemi non inerziali

Se ci ostiniamo ad usare il secondo principio della dinamica **fuori del suo ambito di validità**, possiamo dire che nel sistema non inerziale dobbiamo introdurre termini aggiuntivi detti **forze apparenti**.

$m\mathbf{a^\prime} = \sum \mathbf{F}-m\mathbf{A}$

&nbsp;

Esempio, forza centrifuga...

&nbsp;

NB: attenzione, la formula sopra non è valida in generale, abbiamo evitato il caso in cui il sistema di riferimento non inerziale è in rotazione rispetto a quello inerziale.

---

# Cenni sui sistemi non inerziali

In generale, per un sistema che trasla e ruota

$\displaystyle m \mathbf{a^\prime}=\mathbf{F} -m\mathbf{A} - 2 m \mathbf{\omega}\times \mathbf{v}^\prime -m\left( \frac{d \mathbf{\omega}}{dt} \right)\times \mathbf{r}-m\mathbf{\omega}\times(\mathbf{\omega}\times\mathbf{r})$

(NB: $\mathbf{\omega}$ è un **vettore** che ha il modulo che abbiamo visto, direzione perpendicolare al piano di rotazione, verso positivo per rotazioni antiorarie.)

&nbsp;


<sub> ( Per vedere all'opera l'equazione sopra https://www.youtube.com/watch?v=49JwbrXcPjc  )</sub>

---

# Cenni sui sistemi non inerziali

Per un oggetto che cade verso il centro l'accelerazione di Coriolis è diretta verso est.


![center](coriolis.png)


---

# Cenni sui sistemi non inerziali

Aria che si muove verso una zona di depressione:


![center 100%](Low_pressure_system_over_Iceland.jpg) 

---

# Cenni sui sistemi non inerziali

Andamento delle accelerazioni (frecce rosse) per una massa d'aria che si sposta per effetto di una depressione, sistema di riferimento non inerziale.

![center](504px-Coriolis_effect10.svg.png)

---

# Cenni sui sistemi non inerziali

![center](centrifugal_force.png)

<sub>(from xkcd.com)</sub>

---

# Carrucola

Considereremo in questa prima parte del corso **carrucole ideali**, ossia di **massa trascurabile** e **prive di attrito**.

![40% center](PulleyShip.jpg)

Agisce **cambiando direzione e/o verso ad una forza**.

Attenzione ai sistemi di riferimento!

---

# Carrucola

![75% center](carru1.png)

- Il sistema è all'equilibrio.
- La tensione è la stessa in tutti i punti della corda.

---

# Carrucola

![center](carru2.png)

---

# Macchina di Atwood

![center 150%](Atwd.gif)


---

# Pendolo conico

![center 100%](Conical_pendulum.svg)



---

# Ringraziamenti

Materiale illustrato da:

- Wikipedia

- http://wordpress.mrreid.org/2014/07/16/what-is-the-point-of-a-pulley/

- https://it.wikiversity.org/

- http://hyperphysics.phy-astr.gsu.edu/hbase/index.html
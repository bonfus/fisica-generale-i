# Conservazione dell'energia

---

# Legge di conservazione dell'energia

Una grandezza che mantiene il suo valore costante nel tempo è detta **conservata**.

---

# Forze conservative e non

Il lavoro compiuto da una forza è dato dall'integrale di linea sulla traiettoria percorsa.

In generale quindi **dipende dalla traiettoria!**

---

# Forze conservative e non

Abbiamo incontrato alcuni casi in cui la traiettoria sembra non contare, per esempio nel caso della forza di gravità.

<img src="gravvertical.jpg"><img src="inclinato.png" align="right" width="40%">

---

# Forze conservative e non

&nbsp;

Classifichiamo le forze suddividendole in **conservative** e **non conservative**

**Conservative**: il lavoro totale per un percorso chiuso qualsiasi è nullo.

**Non conservative**: le altre.

&nbsp;

<sub>(In maniera equivalente si può dire che per le forze conservative il lavoro esercitato per andare dal punto $A$ a punto $B$ non dipende dal percorso a meno del segno.)</sub>

---

# Forze centrali

<img src="central.gif" align="right" width="30%">

${\displaystyle {\vec {F}}=\mathbf {F} (\mathbf {r} )=F(\mathbf {r} ){\hat {\mathbf {r} }}}$


La forza è sempre diretta lungo in versore $\mathbf{\hat{r}}$ che congiunge i due punti interagenti.

---

# Forze centrali

![center](gravforcefield.png)

---

# Forze centrali

Le forze centrali sono conservative!

* Ogni curva puo essere approssimata con una serie di archi e spostamenti radiali.
<img src="ccf.gif" align="right" width="30%">

* Per ogni arco ${\displaystyle F(\mathbf {r} ){\hat {\mathbf {r} }}\cdot d\mathbf{s}} = 0$

* L'unico contributo è dato dalla somma di tutti gli spostamenti radiali!

&nbsp;

**Quindi il lavoro fatto dalla forza centrale è indipendente dal percorso!** <sub>(dipende solo dal verso in cui si percorre il percorso)</sub>

Dipende solo dagli estremi. Se gli estremi coincidono, i contributi lungo $\hat {\mathbf {r} }$ e $-\hat {\mathbf {r} }$ si cancellano.

---

# Forze centrali

![center 85%](radiale3.png)
![center 85%](radiale2.png)

---

# Sistema conservativo

Si definisce sistema conservativo il sistema in cui agiscono **solo forze conservative**.

* In un sistema conservativo possono agire più forze conservative.
* Non tutte le forze conservative sono forze centrali.

---

# Prendiamo una palla sulla terra

La forza di gravità per una palla che si muove in su o giù compie il lavoro: $W=-mg(y_f-y_i)$

Dal teorema **lavoro-energia** sappiamo che:

$\frac{1}{2}m v^2 _f - \frac{1}{2}m v^2 _i=-mg(y_f-y_i)$

Riordinando i termini $i$ e $f$

$\displaystyle \color{orange}{\frac{1}{2}m v^2 _f} + \color{blue}{mgy_f} = \color{orange}{\frac{1}{2}m v^2 _i} + \color{blue}{mgy_i}$

La quantità a sinistra, definita al tempo $t_f$ è uguale alla quantità a destra, definita al tempo $t_i$!!


---

# Energia meccanica

&nbsp;

$\displaystyle \color{orange}{\frac{1}{2}m v^2 _f} + \color{blue}{mgy_f} = \color{orange}{\frac{1}{2}m v^2 _i} + \color{blue}{mgy_i}$

&nbsp;

I termini in arancione sono la (nota) **energia cinetica** ($K$).

I termini in blu definiscono una quantità nuova: **energia potenziale gravitazionale** ($U$).

La quantità $E=K+U$ è detta **energia meccanica** e (in questo caso) si conserva!

---

# Considerazioni importanti

&nbsp;

* L'energia potenziale gravitazionale dipende solo dalla "quota".
* L'energia cinetica dipende dal modulo della velocità.

&nbsp;

Possiamo mettere in relazione facilmente queste due quantità nella soluzione dei problemi!

---

# Esempio

Ricordate la formula della quota massima nel moto balistico?

$\displaystyle h= \frac{v_0 ^2 \sin^ 2\theta}{2g}$

![center](hmax.png)

$E=K_i + U_i = K_{\mathrm{h max}}+U_{\mathrm{h max}}$

$\displaystyle E=\frac{1}{2}mv_0 ^2 + 0 = \frac{1}{2}m (v_0  \cos \theta)^2 + mgh$

$\displaystyle \frac{1}{2}mv_0 ^2 (1-\cos^2 \theta) = mgh \;\;\;\; \rightarrow  \;\;\;\;  h=\frac{v_0 ^2 \sin^ 2\theta}{2g}$




---

# Energia potenziale

Definiamo la **variazione dell'energia potenziale** dovuta ad una **forza conservativa** come **l'opposto** del lavoro compiuto dalla forza:


$\displaystyle U_f -U_i= - W = - \int _{x_i} ^{x_f} F_x (x) dx$ &nbsp; &nbsp;(in 1D)

$\displaystyle U_f -U_i= - W = - \int _{i} ^{f} \mathbf{F} \cdot d\mathbf{r}$ &nbsp; &nbsp;(in generale)

&nbsp;

**Solo se il lavoro della forza lungo qualunque cammino chiuso è nullo.**



---

# Conservazione dell'energia meccanica

In un **sistema conservativo**:

$\displaystyle K_f - K_i = W = -( U_f -U_i )$

Riordinando i termini


$\displaystyle \color{Crimson}{ K_f + U_f} = \color{orange}{ K_i  + U_i}$

Ossia, l'energia meccanica $\color{Crimson}{E_f= K_f + U_f}$ al tempo $\color{Crimson}{t_f}$ è uguale all'energia meccanica $\color{orange}{E_i= K_i  + U_i}$ al tempo $\color{orange}{t_i}$, quindi, **nel sistema di forze conservativo**, si conserva!


---

# Conservazione dell'energia meccanica

&nbsp;
&nbsp;
&nbsp;

Ripetiamo: **se a compiere lavoro sono solo forze conservative, l'energia meccanica si conserva**.

&nbsp;

NB: il teorema energia lavoro è stato ricavato a partire dalla seconda legge di Newton quindi questa conclusione è valida solo se facciamo la nostra analisi in un sistema di riferimento inerziale.

&nbsp;
&nbsp;
&nbsp;



---

# Energia potenziale gravitazionale

Abbiamo definito fino ad ora la **variazione** dell'energia potenziale come il lavoro compiuto dalla forza cambiato di segno.

$W=-mg(y_f-y_i)$

$\displaystyle U_f -U_i= -W=mg(y_f-y_i)$

---

# Energia potenziale gravitazionale

$\displaystyle U_f -U_i= -W=mg(y_f-y_i)$

Abbiamo la libertà di prendere un punto di riferimento e fissare l'energia potenziale a zero.

Se poniamo in $U_i=0$ in $y_i=0$, possiamo definire il valore dell'energia potenziale in ogni punto (rispetto a $y=0$)

$\color{Crimson}{U=mgy}$

---

# Esempi

Il valore dipende dalla scelta dell'origine, **le differenze no**!

![center 70%](Figure_08_03_04a.jpg)

$U=0$ a $y_{start}$: 
* $U_{start}=0$,
* $U_{end}=mg(-h)$
* $U_{end}-U_{start}=-mgh$

---

# Esempi

Il valore dipende dalla scelta dell'origine, **le differenze no**!

![center 70%](Figure_08_03_04a.jpg)

$U=0$ a $y_{end}$: 
* $U_{start}=mgh$,
* $U_{end}=0$,
* $U_{end}-U_{start}=-mgh$


---

# Esempi
## Energia potenziale della molla

![center ](molla.jpg)

&nbsp;

$\displaystyle \;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\; U_f -U_i= -W=\frac{1}{2}kx^2 _f -\frac{1}{2}kx^2 _i$

---

# Energia potenziale della molla

Se poniamo $U=0$ in $x_{eq}$,

![center  120%](energiamolla.png)

$\displaystyle \;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\; \color{Crimson}{ U(x)= \frac{1}{2}kx^2}$

---

# Energia potenziale gravitazionale
 
$\displaystyle  W=\int^{r_2}_{r_1} - \frac{GmM}{r^2}\mathbf{\hat{r}}\cdot d\mathbf{r} = -GmM  \int^{r_2}_{r_1} \frac{1}{r^2}dr$

$\displaystyle W=-GmM \left[-\frac{1}{r} \right]^{r_2}_{r_1}=GmM \frac{1}{r_2}-GmM \frac{1}{r_1}$

$\displaystyle U_f -U_i= -W=-GmM \frac{1}{r_2} +GmM \frac{1}{r_1}$

Poniamo convenzionalmente $U_i=0$ in $r_1=\infty$.

---

# Energia potenziale gravitazionale

$\displaystyle U_f -U_i= -W=-GmM \frac{1}{r_2} +GmM \frac{1}{r_1}$

Poniamo convenzionalmente $U_i=0$ in $r_1=\infty$.

$\displaystyle U =-GmM \frac{1}{r}$

![center](u-graph01.jpg)

---

# Energia potenziale gravitazionale

![center](gravpotenergy.png)

---

# Velocità di fuga

A quale velocità dobbiamo lanciare un oggetto per "fuggire" dalla gravità terrestre?

$(K + U_g)_i = (K + U_g)_f$

${\displaystyle \frac {1}{2}}mv_{e}^{2}+{\frac {-GMm}{r}}=0+0$

$\displaystyle v_{e}={\sqrt {\frac {2GM}{r}}}\sim  11 186 \; \mathrm{m/s}$

<sub>I primi sono stati i russi: Luna 1, (Russo: Мечта [mʲɪt͡ɕˈta]) [fonte Wikipedia]</sub>


---

# Forze NON conservative

Un esempio di forza non conservativa è l'attrito!

![center 80%](attrito.png)

In presenza di forze non conservative, **l'energia meccanica non si conserva**!!

---

# Forze non conservative 

Possiamo tuttavia scrivere una versione modificata del teorema dell'energia cinetica.

&nbsp;

$\displaystyle K_f -K_i = W = W_{con} + W_{non}=-(U_f -U_i)+W_{non}$

da cui

$\displaystyle \;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\; K_f + U_f = K_i  + U_i +W_{non}$
$\displaystyle \;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\; E_f \;\;\;\;\;\;\;\;\ = E_i \;\;\;\; \;\;\;\;+W_{non}$

---

# Energia totale

Cosa ne è della conservazione dell’energia in presenza di forze non conservative?

Se agiscono anche forze non conservative,  entrano in gioco altri tipi di energia (ad es.  l’energia termica, l’energia del legame chimico). 

Quando si includono tutte le forme d’energia, l'energia **totale** del sistema si conserva.


---


# Ringraziamenti

Parte del materiale illustrato da

Wikipedia

https://www.mathworks.com/matlabcentral/answers/uploaded_files/149307/image.png

Materiale didattico del Prof. Roberto De Renzi
